let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;
    // console.log(collection);
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    collection[collection.length] = element;
    return collection;
    console.log(collection);
}

function dequeue() {
    // In here you are going to remove the first element in the array
    for (let i = 0; i < collection.length - 1; i++) {
      collection[i] = collection[i + 1];
    };
    --collection.length;
    return collection;
    console.log(collection);
}

function front() {
    // In here, you are going to remove the first element
    let firstElement = collection[0];
    return firstElement;
    console.log(firstElement);

}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   
    let length = 0;
    while (collection[length] !== undefined)
      length++;
    return length;
    console.log(collection);

}

function isEmpty() {
    //it will check whether the function is empty or not
    if(collection.length > 0) {
        condition = false;
    } else {
        condition = true;
    }
    return condition;
    console.log(collection);
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};